package com.example.seroomac.sqliteforeignkey;

import android.app.ActionBar;
import android.app.LoaderManager;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.PermissionChecker;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.seroomac.sqliteforeignkey.data.DatabaseHelper;
import com.example.seroomac.sqliteforeignkey.data.NotContract;
import com.example.seroomac.sqliteforeignkey.data.NotProvider;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

import easyfilepickerdialog.kingfisher.com.library.model.DialogConfig;
import easyfilepickerdialog.kingfisher.com.library.view.FilePickerDialogFragment;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    private FloatingActionMenu fam;
    private FloatingActionButton fabKat;
    private FloatingActionButton fabNot;
    private EditText mUserInput;
    private CategoryListAdapter mCategoryListAdapter;
    private ListView mListView;
    private Uri mSelectedCategoryUri;
    private static final int NOTE_LOADER = 0;
    private static final int CATEGORY_LOADER = 1;
    private String mChosenFilePath;
    private int mEventBusResponse;
//    private ArrayList<DrawerCategories> navigationDrawerList;
    private Toolbar toolbar;
    private ArrayList<PrimaryDrawerItem> primaryDrawerItems;
    Drawer result;


    //FIXME BURADA ONBACKPRESSED OVERRIDE ETMEK LAZIM

    private Boolean exit = false;
    @Override
    public void onBackPressed() {
        if (exit) {
            finish(); // finish activity
        } else {
            Toast.makeText(this, "Press Back again to Exit.",
                    Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);

        }

    }

    /*
        To refresh after import database

     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (resultCode) {
            case RESULT_OK:
                Log.i("ResultOK", "OK");
                getLoaderManager().restartLoader(CATEGORY_LOADER, null, this);

                //Toast.makeText(this, "Oldu gibi", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        super.startActivityForResult(intent, requestCode);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
       if (v.getId() == R.id.list) {
           AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
         //  menu.setHeaderTitle("Category Menu");
           menu.add(0, v.getId(), 0, "Open Category");
           menu.add(0, v.getId(), 0, "Delete Category");
           menu.add(0, v.getId(), 0, "Edit Category Name");
         //  super.onCreateContextMenu(menu, v, menuInfo);
       }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        mSelectedCategoryUri = ContentUris.withAppendedId(NotContract.CategoryEntry.CONTENT_URI, mListView.getItemIdAtPosition((info.position)));
        if (item.getTitle().equals("Open Category")) {
            Intent intent = new Intent(MainActivity.this, InCategoryList.class);
            intent.setData(mSelectedCategoryUri);
            startActivity(intent);
        }
        if (item.getTitle().equals("Delete Category")) {

            showDeleteConfirmationDialog(mSelectedCategoryUri);
        }
        if (item.getTitle().equals("Edit Category Name")) {
            showUpdateCategoryDialog();
        }

        return true;
    }

    private void showDeleteConfirmationDialog(final Uri uri) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Delete Category with all notes in it!");
        builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int id) {
                deleteCategory(uri);
                Toast.makeText(MainActivity.this, "Categori silindi", Toast.LENGTH_LONG).show();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    private void deleteCategory(Uri uri) {

        long selectedCategoryId = getCategoryId();
        long deletedRowCount = deleteAllNotesInDesiredCategory(selectedCategoryId);
        if (deletedRowCount >= 0) {
            Toast.makeText(this, "Notlar Silindi", Toast.LENGTH_SHORT).show();
        }
        long id = ContentUris.parseId(uri);
        String where = NotContract.CategoryEntry._ID + "=?";
        String[] selectionArgs = new String[] {String.valueOf(id)};
        int deleteProcces = getContentResolver().delete(NotContract.CategoryEntry.CONTENT_URI,
                where, selectionArgs);

    }

    private long deleteAllNotesInDesiredCategory(long selectedCategoryId) {
        long rowsDeleted = -1;

      //  String[] projection = {NotContract.NotEntry.}

        String where = NotContract.NotEntry.COLUMN_CAT_ID + "=?";
        String[] selectionArgs = new String[] {String.valueOf(selectedCategoryId)};

        int deleteProcces = getContentResolver().delete(NotContract.NotEntry.CONTENT_URI, where,
                selectionArgs);
        if (deleteProcces > 0) {
            rowsDeleted = deleteProcces;
        }

        return rowsDeleted;
    }
////////*********************--------------------------------------------****************\\\\\\\\\\\\\
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i("Oncreate", "ONCREATE");
        setContentView(R.layout.activity_main);
//        navigationDrawerList = new ArrayList<DrawerCategories>();
        fabKat = findViewById(R.id.fab_kat);
        fabNot = findViewById(R.id.fab_not);
        fam = findViewById(R.id.fab_menu);
        toolbar = findViewById(R.id.toolbarMain);
        primaryDrawerItems = new ArrayList<>();
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setElevation(-1);


        fam.setOnMenuToggleListener(new FloatingActionMenu.OnMenuToggleListener() {
            @Override
            public void onMenuToggle(boolean opened) {
                fabKat.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        LayoutInflater li = LayoutInflater.from(MainActivity.this);
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                MainActivity.this);


                        mUserInput = new EditText(MainActivity.this);
                        alertDialogBuilder.setView(mUserInput);

                        alertDialogBuilder
                                .setCancelable(false)
                                .setPositiveButton("OK",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog,int id) {
                                                String categoryName = mUserInput.getText().toString();
                                                if (categoryName.equals("")) {
                                                    return;
                                                }
                                                saveCategory(categoryName);
                                                fam.close(true);
                                            }
                                        })
                                .setNegativeButton("Cancel",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog,int id) {
                                                dialog.cancel();
                                            }
                                        });
                        alertDialogBuilder.show();


                    }
                });
                fabNot.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        
                        if (mCategoryListAdapter.isEmpty()) {
                            Toast.makeText(MainActivity.this, "Lutfen bir kategory ekleyiniz",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            Intent intent = new Intent(MainActivity.this, NotAddActivity.class);
                            startActivity(intent);
                        }

                    }
                });
            }

        });

        mListView = findViewById(R.id.list);
        mCategoryListAdapter = new CategoryListAdapter(this, null);

        mListView.setAdapter(mCategoryListAdapter);
        registerForContextMenu(mListView);



        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, InCategoryList.class);
                Uri currentCatUri = ContentUris.withAppendedId(NotContract.CategoryEntry.CONTENT_URI, id);
                Log.i("IDDD", String.valueOf(id));
                intent.setData(currentCatUri);
                startActivity(intent);
            }
        });

        getLoaderManager().initLoader(CATEGORY_LOADER, null, this);

        //*****-------------------------------------------------------------------****\\\


        // Create the AccountHeader
        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.drawable.navheader)
                .addProfiles(
                        new ProfileDrawerItem().withName("NoteApp"))
                .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
                    @Override
                    public boolean onProfileChanged(View view, IProfile profile, boolean currentProfile) {
                        return false;
                    }
                })
                .build();


//create the drawer and remember the `Drawer` result object
        result = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withAccountHeader(headerResult)
                .addDrawerItems(
                        new PrimaryDrawerItem().withIdentifier(1).withName("Back-Up All Notes"),
                        new PrimaryDrawerItem().withIdentifier(2).withName("Import a backup file"),
                        new PrimaryDrawerItem().withIdentifier(3).withName("Password Security Option")
                )
                .withOnDrawerItemClickListener(new com.mikepenz.materialdrawer.Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        //FIXME drawer ok. navigation drawerlist ten idleri alarak asagiyi doldur.
                        switch (position){
                            case 1:
                                backupDataBase();
                                break;
                            case 2:
                                int REQUEST_CODE = 12345;
                                Intent intent = new Intent(MainActivity.this, ImportDatabase.class);
                                startActivityForResult(intent, REQUEST_CODE);
                                break;
                            case 3:
                                intent = new Intent(MainActivity.this, SetPassword.class);
                                startActivity(intent);
                                break;
                        }


                        return true;
                    }
                })
                .build();





    } //end of the oncreate

//    private void createDrawerItems() {
//        for (int i = 0; i < navigationDrawerList.size(); i++) {
//            String name = navigationDrawerList.get(i).getName();
//            primaryDrawerItems.add(new PrimaryDrawerItem().withIdentifier(i + 1).withName(name));
//            result.openDrawer();
//            result.addItem(primaryDrawerItems.get(i));
//            result.closeDrawer();
//            Log.i("DRAWERITEMS", String.valueOf(navigationDrawerList.get(i).getId()));
//        }
//
//
//    }

//    private class DrawerCategories {
//        private long id;
//        private String name;
//
//        private DrawerCategories(long id, String name) {
//            this.id = id;
//            this.name = name;
//        }
//        private long getId() {
//            return id;
//        }
//        private String getName() {
//            return name;
//        }
//    }

    private void saveCategory(String categoryName) {

        ContentValues values = new ContentValues();
        if (categoryName == null && TextUtils.isEmpty(categoryName)) {
            return;
        }

        values.put(NotContract.CategoryEntry.COLUMN_CATEGORY_NAME, categoryName);

            Uri newUri = getContentResolver().insert(NotContract.CategoryEntry.CONTENT_URI, values);

    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        switch (id) {
            case CATEGORY_LOADER:
                String[] projection = {
                        NotContract.CategoryEntry._ID,
                        NotContract.CategoryEntry.COLUMN_CATEGORY_NAME,
                };
                return new CursorLoader(this, NotContract.CategoryEntry.CONTENT_URI,
                        projection, null, null, null);
            default:
                Log.e("OnCreateLoader", "Hata");
        }

        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

//        if (data.moveToFirst()) {
//            do{
//                String catName = data.getString(data.getColumnIndex(NotContract.CategoryEntry.COLUMN_CATEGORY_NAME));
//                long catId = data.getLong(data.getColumnIndex(NotContract.CategoryEntry._ID));
//                navigationDrawerList.add(new DrawerCategories(catId, catName));
//            } while(data.moveToNext());
//        }
        mCategoryListAdapter.swapCursor(data);
//        createDrawerItems();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mCategoryListAdapter.swapCursor(null);
    }

    private long getCategoryId() {
        long categoryId = -1;
        String[] projection = {
                NotContract.CategoryEntry._ID,
                NotContract.CategoryEntry.COLUMN_CATEGORY_NAME,
        };
        String selection = NotContract.CategoryEntry._ID + "=?";
        String[] selectionArgs = new String[] {String.valueOf(ContentUris.
                parseId(mSelectedCategoryUri))};

        Cursor cursor = getContentResolver().query(NotContract.CategoryEntry.CONTENT_URI,
                projection, selection, selectionArgs, null);

        if (cursor.moveToFirst()) {
            int catIdIndex = cursor.getColumnIndex(NotContract.CategoryEntry._ID);
            categoryId = cursor.getLong(catIdIndex);
            int catNameIndex = cursor.getColumnIndex(NotContract.CategoryEntry.COLUMN_CATEGORY_NAME);
            String categoryName = cursor.getString(catNameIndex);
        }
        return categoryId;
    }

    public void showUpdateCategoryDialog() {
        LayoutInflater li = LayoutInflater.from(MainActivity.this);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                MainActivity.this);


        mUserInput = new EditText(MainActivity.this);
        alertDialogBuilder.setView(mUserInput);

        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                String categoryName = mUserInput.getText().toString();

                                updateCategory(categoryName);

                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });
        alertDialogBuilder.show();

    }

    private void updateCategory(String categoryName) {

        ContentValues values = new ContentValues();
        values.put(NotContract.CategoryEntry.COLUMN_CATEGORY_NAME, categoryName);

        long id = ContentUris.parseId(mSelectedCategoryUri);
        String where = NotContract.CategoryEntry._ID + "=?";
        String[] selectionArgs = new String[] {String.valueOf(id)};

        int rowsAffected = getContentResolver().update(NotContract.CategoryEntry.CONTENT_URI, values, where, selectionArgs);
        if (rowsAffected > 0) {
            Toast.makeText(this, "Categori Adı Değiştirildi", Toast.LENGTH_SHORT).show();
        }

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu options from the res/menu/menu_editor.xml file.
        // This adds menu items to the app bar.
        getMenuInflater().inflate(R.menu.options_menu_main, menu);
        return true;
    }
    //*************************************-------------------***************\\\\\\\\\\\
    public boolean onOptionsItemSelected(MenuItem item) {
        // User clicked on a menu option in the app bar overflow menu
        Intent intent;
        switch (item.getItemId()) {
            // Respond to a click on the "Save" menu option
            case R.id.action_backup:
                backupDataBase();
                return true;
            // Respond to a click on the "Delete" menu option
            case R.id.import_database:
                
                int REQUEST_CODE = 12345;
                intent = new Intent(MainActivity.this, ImportDatabase.class);
                startActivityForResult(intent, REQUEST_CODE);
                return true;
            case R.id.password_security:
                intent = new Intent(MainActivity.this, SetPassword.class);
                startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    private void copyNewDatabase(String newBackupFilePath) {
        try {
            File targetDbPathFile = Environment.getDataDirectory();
            File targetDbFile = new File(targetDbPathFile, DatabaseHelper.DB_FILE_PATH);//mevcut .db dosyasi
            File choosenFile = new File(newBackupFilePath); //secilen .db dosyası
            Log.e("BEFORE FILE COPY", newBackupFilePath);
            FileChannel src = new FileInputStream(choosenFile).getChannel();
            //FIXME burada hala diske yazma izni yok elle izin verip burayi geciyorum.. daha sonra duzelt buralari
            FileChannel dst = new FileOutputStream(targetDbFile).getChannel();
            dst.transferFrom(src, 0, src.size());
            src.close();
            dst.close();
            Toast.makeText(getBaseContext(), "Successfully import Database", Toast.LENGTH_SHORT).show();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
            Log.e("ERROR", ex.getStackTrace().toString());
        }
    }

    private String getNewFilePath() {
        final String[] s = new String[1];
        DialogConfig dialogConfig = new DialogConfig.Builder()
                //.supportFiles(new SupportFile(".db", R.drawable.ic_file))
                .build();

        new FilePickerDialogFragment.Builder()
                .configs(dialogConfig)
                .onFilesSelected(new FilePickerDialogFragment.OnFilesSelectedListener() {
                     @Override
                     public void onFileSelected(List<File> list) {
                         //FIXME buraya girmiyor hala sikinti büyük..prefe yazmayı deneyebilirim..ya da ayri activityde yapabilirim.

                         s[0] = list.get(0).getAbsolutePath();
                         Log.e("ONFILESELECTED", s[0]);
                       //  Toast.makeText(MainActivity.this, mChosenFilePath, Toast.LENGTH_LONG).show();
                     }
                 }).build().show(getSupportFragmentManager(), null);
        Log.e("BEFORE RETURN", s[0]);
                return s[0];


    }

    private void backupDataBase() {
        try {
            File sd = new File("/storage/emulated/0/Download/");
            File data = Environment.getDataDirectory();
           // if (sd.canWrite()) {

                String backupDBPath = "noteAppDataBaseBackup.db";
                File currentDB = new File(data, DatabaseHelper.DB_FILE_PATH);
                File backupDB = new File(sd, backupDBPath);
            //FIXME olmus-- burada hala diske yazma izni yok elle izin verip burayi geciyorum.. daha sonra duzelt buralari

                FileChannel src = new FileInputStream(currentDB).getChannel();
                FileChannel dst = new FileOutputStream(backupDB).getChannel();
                dst.transferFrom(src, 0, src.size());
                src.close();
                dst.close();
                Toast.makeText(getBaseContext(), "Successfully backed up database", Toast.LENGTH_SHORT).show();

          //  }
        } catch (IOException e) {
            Toast.makeText(getBaseContext(), "Error backing up database", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

    }


    @Override
    protected void onRestart() {
        super.onRestart();
        int response = mEventBusResponse;
        if (mEventBusResponse == 1) {
            // do nothing
        } else {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            EventBus.getDefault().unregister(this);
            mEventBusResponse = 0;
        }

        Log.i("Onrestart", "ONRESTART");
        EventBus.getDefault().unregister(this);
        mEventBusResponse = 0;
    }


    @Subscribe
    public void getResponse(MessageEvent.Response event) {
        mEventBusResponse = event.getIsFromInApp();
    }
    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        Log.i("Onstart", "ONSTART");
    }

   /* @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
        Log.i("Onstop", "ONSTOP");
    }*/

    /*@Override
    protected void onPause() {
        super.onPause();
        Log.i("Onpause", "ONPAUSE");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("Ondestroy", "ONDESTROY");
    }@Override
    protected void onRestart() {
        super.onRestart();
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        Log.i("Onrestart", "ONRESTART");
    }



*/


  /*  @Override
    protected void onResume() {
        super.onResume();
        Log.i("Onresume", "ONRESUME");
    }




    @Override
    protected void onPostResume() {
        super.onPostResume();
        Log.i("Onpostresume", "ONPOSTRESUME");
    }*/
}


