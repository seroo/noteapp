package com.example.seroomac.sqliteforeignkey;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    EditText mEt1;
    EditText mEt2;
    EditText mEt3;
    EditText mEt4;
    Button mBtn;
    LinearLayout container;
    int mTypedPass;
    Intent mIntent;
    SharedPreferences mPrefs;
    SharedPreferences.Editor mPrefEditor;
    final int DEFAULT_PASS = -1;
    final String SHAREDPREFS_PASS_KEY = "pass";
    private long backPressedTime = 0;

   /* @Override
    protected void onDestroy() {
        super.onDestroy();
    }*/





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mEt1 = (EditText) findViewById(R.id.et1);
        mEt2 = (EditText) findViewById(R.id.et2);
        mEt3 = (EditText) findViewById(R.id.et3);
        mEt4 = (EditText) findViewById(R.id.et4);
        mBtn = findViewById(R.id.password_button);
        container = findViewById(R.id.password_container);
        container.setVisibility(View.INVISIBLE);
        mIntent = new Intent(this, MainActivity.class);
        mPrefs = getSharedPreferences(getPackageName() + ".util.xml", MODE_PRIVATE);
        mPrefEditor = mPrefs.edit();
        int storedPass = mPrefs.getInt(SHAREDPREFS_PASS_KEY, DEFAULT_PASS);
        if (storedPass == DEFAULT_PASS) {
            startActivity(mIntent);
            finish();
        } else {
            container.setVisibility(View.VISIBLE);
            //edittext secili olsun diye
            Selection.setSelection((Editable) mEt1.getText(), mEt1.getSelectionStart());
            mEt1.requestFocus();
            //klavyeyi acmak icin
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }


        mEt1.addTextChangedListener(new TextWatcher() {
            int firstSize = 0;
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                firstSize = mEt1.getText().toString().length();
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void afterTextChanged(Editable editable) {
                if (firstSize > mEt1.getText().toString().length()) {
                    //do nothing
                } else {
                    Selection.setSelection((Editable) mEt2.getText(), mEt2.getSelectionStart());
                    mEt2.requestFocus();
                }

            }
        });
            mEt2.addTextChangedListener(new TextWatcher() {
                int firstSize = 0;
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    firstSize = mEt2.getText().toString().length();
                }
                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }
                @Override
                public void afterTextChanged(Editable editable) {
                    if (firstSize > mEt2.getText().toString().length()) {
                        //do nothing
                    } else {
                        Selection.setSelection((Editable) mEt3.getText(), mEt3.getSelectionStart());
                        mEt3.requestFocus();
                    }

                }
            });
            mEt3.addTextChangedListener(new TextWatcher() {
                int firstSize = 0;
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    firstSize = mEt3.getText().toString().length();
                }
                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
                @Override
                public void afterTextChanged(Editable editable) {
                    if (firstSize > mEt3.getText().toString().length()) {
                        //do nothing
                    } else {
                        Selection.setSelection((Editable) mEt4.getText(), mEt4.getSelectionStart());
                        mEt4.requestFocus();
                    }
                }
            });
            mEt4.addTextChangedListener(new TextWatcher() {
                int firstSize = 0;
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    firstSize = mEt4.getText().toString().length();
                }
                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
                @Override
                public void afterTextChanged(Editable editable) {
                    if (firstSize > mEt4.getText().toString().length()) {
                        //do nothing
                    } else {
                        boolean isCorrectPass = controlPass();
                        if (isCorrectPass) {
                            //container.setVisibility(View.INVISIBLE);
                            mEt1.getText().clear();
                            mEt2.getText().clear();
                            mEt3.getText().clear();
                            mEt4.getText().clear();
                            Selection.setSelection((Editable) mEt1.getText(), mEt1.getSelectionStart());
                            mEt1.requestFocus();
                            startActivity(mIntent);
                            finish();

                        } else {
                            String s = mEt1.getText().toString() + mEt2.getText().toString() +
                                    mEt3.getText().toString() + mEt4.getText().toString();
                            Toast.makeText(LoginActivity.this, s, Toast.LENGTH_LONG).show();
                            mEt1.getText().clear();
                            mEt2.getText().clear();
                            mEt3.getText().clear();
                            mEt4.getText().clear();
                            Selection.setSelection((Editable) mEt1.getText(), mEt1.getSelectionStart());
                            mEt1.requestFocus();
                        }
                    }

                }
            });



    }

    private boolean controlPass() {
        int typedPass = Integer.parseInt(mEt1.getText().toString() + mEt2.getText().toString()
                + mEt3.getText().toString() + mEt4.getText().toString());

        if (typedPass == mPrefs.getInt(SHAREDPREFS_PASS_KEY, DEFAULT_PASS)) {
            return true;
        } else
            return false;
    }
}
