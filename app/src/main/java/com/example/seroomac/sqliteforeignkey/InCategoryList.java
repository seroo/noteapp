package com.example.seroomac.sqliteforeignkey;

import android.app.LoaderManager;
import android.content.ContentUris;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.seroomac.sqliteforeignkey.data.NotContract;
import com.github.clans.fab.FloatingActionButton;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class InCategoryList extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    FloatingActionButton fab;
    private int mEventBusResponse;
    InCategoryListAdapter mInCategoryListAdapter;

    Uri mCurrentCategoryUri;
    private static final int NOTE_LOADER = 0;
    private static final int CATEGORY_LOADER = 1;
    ListView mListView;
    String mCurrentCategoryName = "";
    TextView mTvActivityHeader;

    @Override
    protected void onRestart() {
        super.onRestart();
        int response = mEventBusResponse;
        if (mEventBusResponse == 1) {
            // do nothing
        } else {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            EventBus.getDefault().unregister(this);
            mEventBusResponse = 0;
        }

        Log.i("Onrestart", "ONRESTART");
        EventBus.getDefault().unregister(this);
        mEventBusResponse = 0;
    }

    @Subscribe
    public void getResponse(MessageEvent.Response event) {
        mEventBusResponse = event.getIsFromInApp();
    }
    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        Log.i("Onstart", "ONSTART");
    }

 /*   @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
        Log.i("Onstop", "ONSTOP");
    }*/

    @Override
    public void onBackPressed() {
        EventBus.getDefault().post(new MessageEvent.Response(1));
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_in_category_list);

        Intent intent= getIntent();
        mCurrentCategoryUri = intent.getData();
        mCurrentCategoryName = getCategoryName();
        mTvActivityHeader = findViewById(R.id.in_category_list_activty_header);
        mTvActivityHeader.setText(mTvActivityHeader.getText().toString() + " - " +
                mCurrentCategoryName);
        //Bu uri den gelen categori id ye ait notlari listelicez
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_in_category);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(InCategoryList.this, NotAddActivity.class);
                intent.putExtra("categoryName", mCurrentCategoryName);
                intent.setData(mCurrentCategoryUri);
                startActivity(intent);
            }
        });

        mListView = findViewById(R.id.category_list);
        mInCategoryListAdapter = new InCategoryListAdapter(this, null);

        mListView.setAdapter(mInCategoryListAdapter);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent intent = new Intent(InCategoryList.this, NotAddActivity.class);
                Uri currentNoteUri = ContentUris.withAppendedId(NotContract.NotEntry.CONTENT_URI, id);
                intent.setData(currentNoteUri);
                startActivity(intent);
            }
        });

        getLoaderManager().initLoader(NOTE_LOADER, null, this );
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        String[] projection = {

                NotContract.NotEntry._ID,
                NotContract.NotEntry.COLUMN_HEADER,
                NotContract.NotEntry.COLUMN_DATE
        };
        String selection = NotContract.NotEntry.COLUMN_CAT_ID + "=?";
        String[] selectionArgs = new String[]{getCategoryId()};
        return new CursorLoader(this, NotContract.NotEntry.CONTENT_URI,
                projection, selection, selectionArgs, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
      mInCategoryListAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mInCategoryListAdapter.swapCursor(null);
    }

    private String getCategoryId() {
        String categoryId = "-1";
        String[] projection = {
                NotContract.CategoryEntry._ID,
                NotContract.CategoryEntry.COLUMN_CATEGORY_NAME,
        };
        String selection = NotContract.CategoryEntry._ID + "=?";
        String[] selectionArgs = new String[] {String.valueOf(ContentUris.
                parseId(mCurrentCategoryUri))};

       Cursor cursor = getContentResolver().query(NotContract.CategoryEntry.CONTENT_URI,
               projection, selection, selectionArgs, null);

       if (cursor.moveToFirst()) {
           int catIdIndex = cursor.getColumnIndex(NotContract.CategoryEntry._ID);
           categoryId = String.valueOf(cursor.getLong(catIdIndex));
           int catNameIndex = cursor.getColumnIndex(NotContract.CategoryEntry.COLUMN_CATEGORY_NAME);
           String categoryName = cursor.getString(catNameIndex);
       }
        return categoryId;
    }

    private String getCategoryName() {
        String categoryId = "-1";
        String categoryName = "";
        String[] projection = {
                NotContract.CategoryEntry._ID,
                NotContract.CategoryEntry.COLUMN_CATEGORY_NAME,
        };
        String selection = NotContract.CategoryEntry._ID + "=?";
        String[] selectionArgs = new String[] {String.valueOf(ContentUris.
                parseId(mCurrentCategoryUri))};

        Cursor cursor = getContentResolver().query(NotContract.CategoryEntry.CONTENT_URI,
                projection, selection, selectionArgs, null);

        if (cursor.moveToFirst()) {
            int catIdIndex = cursor.getColumnIndex(NotContract.CategoryEntry._ID);
            categoryId = String.valueOf(cursor.getLong(catIdIndex));
            int catNameIndex = cursor.getColumnIndex(NotContract.CategoryEntry.COLUMN_CATEGORY_NAME);
            categoryName = cursor.getString(catNameIndex);
        }
        return categoryName;
    }

}
