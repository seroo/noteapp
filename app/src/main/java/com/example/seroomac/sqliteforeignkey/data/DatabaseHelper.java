package com.example.seroomac.sqliteforeignkey.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.example.seroomac.sqliteforeignkey.data.NotContract.NotEntry;
import com.example.seroomac.sqliteforeignkey.data.NotContract.CategoryEntry;

import java.io.IOException;


/**
 * Created by seroomac on 25.01.2018.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "notlar.db";
    public static final String DB_FILE_PATH = "/data/com.example.seroomac.sqliteforeignkey/databases/notlar.db";

    public static final int DATABASE_VERSION = 3;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static final String CREATE_CATEGORY_TABLE = "CREATE TABLE "
            + CategoryEntry.TABLE_NAME + " (" + CategoryEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + CategoryEntry.COLUMN_CATEGORY_NAME + " TEXT NOT NULL);";


    public static final String CREATE_NOTE_TABLE = "CREATE TABLE "
            + NotEntry.TABLE_NAME + "("
            + NotEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + NotEntry.COLUMN_HEADER + " TEXT, "
            + NotEntry.COLUMN_CONTENT + " TEXT, "
            + NotEntry.COLUMN_CAT_ID + " INTEGER, "
            + NotEntry.COLUMN_DATE + " INTEGER, "
            + "FOREIGN KEY (" + NotEntry.COLUMN_CAT_ID + ")" + " REFERENCES " + CategoryEntry.TABLE_NAME
            + " (" + CategoryEntry._ID + ")" + ");";

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_CATEGORY_TABLE);
        db.execSQL(CREATE_NOTE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + CategoryEntry.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + NotEntry.TABLE_NAME);
        onCreate(db);
    }

    public boolean importDataBase(String dbPath) throws IOException {

        

        return false;
    }
}

