package com.example.seroomac.sqliteforeignkey;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.example.seroomac.sqliteforeignkey.data.NotContract;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by Elitgd on 30.01.2018.
 */

public class InCategoryListAdapter extends CursorAdapter {

    public InCategoryListAdapter(Context context, Cursor c) {

        super(context, c, 0);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.in_category_list_item, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView tvNotHeader = view.findViewById(R.id.note_header);
        TextView tvNotDate = view.findViewById(R.id.note_date);


        String noteHeader = cursor.getString(cursor.getColumnIndexOrThrow(
                NotContract.NotEntry.COLUMN_HEADER
        ));
        Long noteDate = cursor.getLong(cursor.getColumnIndexOrThrow(
                NotContract.NotEntry.COLUMN_DATE
        ));
        Calendar c = new GregorianCalendar();
        SimpleDateFormat s = new SimpleDateFormat("dd/MM/yyy - HH:mm");
        c.setTimeInMillis(noteDate);
        s.setCalendar(c);
        String dateInString = s.format(c.getTime());
        tvNotHeader.setText(noteHeader);
        tvNotDate.setText(dateInString);

    }


}
