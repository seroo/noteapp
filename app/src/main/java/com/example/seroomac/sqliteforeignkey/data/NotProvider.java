package com.example.seroomac.sqliteforeignkey.data;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.seroomac.sqliteforeignkey.data.NotContract.NotEntry;
import com.example.seroomac.sqliteforeignkey.data.NotContract.CategoryEntry;

/**
 * Created by seroomac on 25.01.2018.
 */

public class NotProvider extends ContentProvider {

    public static final String TAG = NotProvider.class.getSimpleName();

    private static final int NOTES = 100;
    private static final int NOTE_ID = 101;
    private static final int CATEGORIES = 200;
    private static final int CAT_ID = 201;

    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        sUriMatcher.addURI(NotContract.CONTENT_AUTHORITY, NotContract.PATH_NOT, NOTES);
        sUriMatcher.addURI(NotContract.CONTENT_AUTHORITY, NotContract.PATH_NOT + "/#", NOTE_ID);
        sUriMatcher.addURI(NotContract.CONTENT_AUTHORITY, NotContract.PATH_CAT, CATEGORIES);
        sUriMatcher.addURI(NotContract.CONTENT_AUTHORITY, NotContract.PATH_CAT + "/#", CAT_ID);
    }

    private DatabaseHelper mDatabaseHelper;

    @Override
    public boolean onCreate() {

        mDatabaseHelper = new DatabaseHelper(getContext());

        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection,
                        @Nullable String[] selectionArgs, @Nullable String sortOrder) {

        SQLiteDatabase database = mDatabaseHelper.getReadableDatabase();
        Cursor cursor;

        int match = sUriMatcher.match(uri);
        switch (match) {
            case NOTES:
                cursor = database.query(NotEntry.TABLE_NAME, projection, selection, selectionArgs,
                        null, null, sortOrder);
                break;
            case NOTE_ID:
                selection = NotEntry._ID + "=?";
                selectionArgs = new String[] {String.valueOf(ContentUris.parseId(uri))};
                cursor = database.query(NotEntry.TABLE_NAME, projection, selection, selectionArgs,
                        null, null, sortOrder);
                break;
            case CATEGORIES:
                cursor = database.query(CategoryEntry.TABLE_NAME, projection, selection, selectionArgs,
                        null, null, sortOrder);
                break;
            case CAT_ID:
                selection = CategoryEntry._ID + "=?";
                selectionArgs = new String[] {String.valueOf(ContentUris.parseId(uri))};
                cursor = database.query(CategoryEntry.TABLE_NAME, projection, selection, selectionArgs,
                        null, null, sortOrder);
                break;
            default:
                throw new IllegalArgumentException("Cant query unknown Uri " + uri);

        }
        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case NOTES: return NotEntry.CONTENT_LIST_TYPE;
            case NOTE_ID: return NotEntry.CONTENT_ITEM_TYPE;
            case CATEGORIES: return CategoryEntry.CONTENT_LIST_TYPE;
            case CAT_ID: return CategoryEntry.CONTENT_ITEM_TYPE;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri + " with match " + match);
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case NOTES:
                return insertNot(uri, contentValues);
            case CATEGORIES:
                return insertCat(uri, contentValues);
            default:
                throw new IllegalArgumentException("Insertion is not supported for " + uri);
        }
    }

    private Uri insertCat(Uri uri, ContentValues values) {

      //  checkExeption(values);
        long id = -1;
        SQLiteDatabase database = mDatabaseHelper.getWritableDatabase();

        long newRowId = database.insert(CategoryEntry.TABLE_NAME, null, values);
        if (newRowId == -1) {
            Log.v("CatProvider", "Kaydedilemedi");
        } else {
            Log.v("CatProvider", "Kaydedildi");
        }

        //veri degisirse uridaki veriyi degistirecek galiba
        //querydeki gibi aslinda degisiklik yapan metodlarin hepsinde bu metod olacak
        getContext().getContentResolver().notifyChange(uri, null);

        //  id = newRowId;
        // Once we know the ID of the new row in the table,
        // return the new URI with the ID appended to the end of it
        return ContentUris.withAppendedId(uri, id);

    }

    private Uri insertNot(Uri uri, ContentValues values) {

       // checkExeption(values); sonra duzeltilecek
        long id = -1;
        SQLiteDatabase database = mDatabaseHelper.getWritableDatabase();
        // PetQueryHandler petHandler = new PetQueryHandler(this.getContext().getContentResolver());

        long newRowId = database.insert(NotEntry.TABLE_NAME, null, values);
        if (newRowId == -1) {
            Log.v("NotProvider", "Kaydedilemedi");
        } else {
            Log.v("NotProvider", "Kaydedildi");
        }

        //veri degisirse uridaki veriyi degistirecek galiba
        //querydeki gibi aslinda degisiklik yapan metodlarin hepsinde bu metod olacak
        getContext().getContentResolver().notifyChange(uri, null);

        //  id = newRowId;
        // Once we know the ID of the new row in the table,
        // return the new URI with the ID appended to the end of it
        return ContentUris.withAppendedId(uri, id);

    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {

        SQLiteDatabase database = mDatabaseHelper.getWritableDatabase();
        int rowsDeleted;
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case NOTES:
                // Delete all rows that match the selection and selection args

                rowsDeleted = database.delete(NotEntry.TABLE_NAME, selection, selectionArgs);
                if (rowsDeleted != 0) {
                    getContext().getContentResolver().notifyChange(uri, null);
                }
                return rowsDeleted;
            case NOTE_ID:
                // Delete a single row given by the ID in the URI
                selection = NotEntry._ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                rowsDeleted = database.delete(NotEntry.TABLE_NAME, selection, selectionArgs);
                if (rowsDeleted != 0) {
                    //degisiklik olursa query e bildir
                    getContext().getContentResolver().notifyChange(uri, null);
                }
                return rowsDeleted;
            case CATEGORIES:
                // Delete all rows that match the selection and selection args

                rowsDeleted = database.delete(CategoryEntry.TABLE_NAME, selection, selectionArgs);
                if (rowsDeleted != 0) {
                    getContext().getContentResolver().notifyChange(uri, null);
                }
                return rowsDeleted;
            case CAT_ID:
                // Delete a single row given by the ID in the URI
                selection = CategoryEntry._ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                rowsDeleted = database.delete(CategoryEntry.TABLE_NAME, selection, selectionArgs);
                if (rowsDeleted != 0) {
                    //degisiklik olursa query e bildir
                    getContext().getContentResolver().notifyChange(uri, null);
                }
                return rowsDeleted;
            default:
                throw new IllegalArgumentException("Deletion is not supported for " + uri);
        }
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String selection,
                      @Nullable String[] selectionArgs) {

        final int match = sUriMatcher.match(uri);
        switch (match) {
            case NOTES:
                return updateEntry(uri, contentValues, selection, selectionArgs);
            case NOTE_ID:
                // For the PET_ID code, extract out the ID from the URI,
                // so we know which row to update. Selection will be "_id=?" and selection
                // arguments will be a String array containing the actual ID.
                selection = NotEntry._ID + "=?";
                selectionArgs = new String[] { String.valueOf(ContentUris.parseId(uri)) };
                return updateEntry(uri, contentValues, selection, selectionArgs);
            case CATEGORIES:
                return updateEntry(uri, contentValues, selection, selectionArgs);
            case CAT_ID:
                // For the PET_ID code, extract out the ID from the URI,
                // so we know which row to update. Selection will be "_id=?" and selection
                // arguments will be a String array containing the actual ID.
                selection = CategoryEntry._ID + "=?";
                selectionArgs = new String[] { String.valueOf(ContentUris.parseId(uri)) };
                return updateEntry(uri, contentValues, selection, selectionArgs);
            default:
                throw new IllegalArgumentException("Update is not supported for " + uri);
        }
    }

    private int updateEntry(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        if (values.size() == 0) {
            return 0;
        }
        int rowsUpdated;

        SQLiteDatabase database = mDatabaseHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case NOTES:
                rowsUpdated = database.update(NotEntry.TABLE_NAME, values,
                        selection, selectionArgs);
                break;
                case NOTE_ID:
                rowsUpdated = database.update(NotEntry.TABLE_NAME, values,
                        selection, selectionArgs);
            break;
            case CATEGORIES:
                rowsUpdated = database.update(CategoryEntry.TABLE_NAME, values,
                        selection, selectionArgs);
                break;
            case CAT_ID:
                rowsUpdated = database.update(CategoryEntry.TABLE_NAME, values,
                        selection, selectionArgs);
                break;
            default:
                rowsUpdated = 0;
        }

        if (rowsUpdated != 0) {
            //degisiklik olursa bildir
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsUpdated;

    }

    //FIXME sonra duzelt buraları
    private void checkExeption (ContentValues values) {
        String catId = values.getAsString(NotEntry.COLUMN_CAT_ID);
        if (catId == null) {
            throw new IllegalArgumentException("Requires a categoryID");
        }
    }
}
