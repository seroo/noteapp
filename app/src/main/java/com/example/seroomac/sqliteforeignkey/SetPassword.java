package com.example.seroomac.sqliteforeignkey;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.w3c.dom.Text;

public class SetPassword extends AppCompatActivity {
    //FIXME PASSWORD AYARINDAN SONRA GERİ LOGIN ACTİVİTY E DONUYOR ÇARE BULMAK LAZIM
    Switch mOnOfSwitch;
    EditText mEtSetPassword;
    Button mSetPassButton;
    TextView mTvWrongPass;
    int mCount;
    SharedPreferences mSharedPrefs;
    SharedPreferences.Editor mPrefEditor;
    final String SHAREDPREFS_PASS_KEY = "pass";
    final int DEFAULT_PASS = -1;
    int mTypedPass1 = 0;
    int mTypedPass2 = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_password);
        mSharedPrefs = getSharedPreferences(getPackageName() + ".util.xml", MODE_PRIVATE);
        mPrefEditor = mSharedPrefs.edit();
        mEtSetPassword = (EditText) findViewById(R.id.et_set_password);
        mOnOfSwitch = findViewById(R.id.switch_password_on_of);
        mTvWrongPass = findViewById(R.id.tv_wrong_pass);
        mSetPassButton = findViewById(R.id.btn_set_password);
        if (mSharedPrefs.getInt(SHAREDPREFS_PASS_KEY, DEFAULT_PASS) > DEFAULT_PASS) {
            mOnOfSwitch.setChecked(true);
        }

        mOnOfSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked) {
                    mEtSetPassword.setVisibility(View.VISIBLE);
                    mSetPassButton.setVisibility(View.VISIBLE);
                    Selection.setSelection((Editable) mEtSetPassword.getText(), mEtSetPassword.getSelectionStart());
                    mEtSetPassword.requestFocus();
                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                } else {
                    mPrefEditor.clear().apply();
                    mEtSetPassword.setVisibility(View.INVISIBLE);
                    mSetPassButton.setVisibility(View.INVISIBLE);
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                    EventBus.getDefault().post(new MessageEvent.Response(1));
                    finish();
                    //FIXME burada tekrar sifre sordurmak lazim oyle direk password kaldırma olmaz.
                }
            }
        });

        mEtSetPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int count) {
               // Toast.makeText(SetPassword.this, i + "", Toast.LENGTH_SHORT).show();

                if ((i + 1) == 4) {
                    mSetPassButton.setEnabled(true);
                    return;
                } else {
                    return;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        mSetPassButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCount == 0) {
                    mTypedPass1 = Integer.parseInt(mEtSetPassword.getText().toString());
                    mEtSetPassword.getText().clear();
                    mSetPassButton.setEnabled(false);
                    mCount++;
                } else {
                    mTypedPass2 = Integer.parseInt(mEtSetPassword.getText().toString());
                    if (mTypedPass1 == mTypedPass2) {
                        mPrefEditor.putInt(SHAREDPREFS_PASS_KEY, mTypedPass1);
                        mPrefEditor.apply();
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        EventBus.getDefault().post(new MessageEvent.Response(1));
                        finish();
                    } else {
                        mTvWrongPass.setText("NOT MATCH SET AGAIN");
                        mCount = 0;
                        mTypedPass1 = 0;
                        mTypedPass2 = -1;
                        mEtSetPassword.getText().clear();
                        Selection.setSelection((Editable) mEtSetPassword.getText(), mEtSetPassword.getSelectionStart());
                        mEtSetPassword.requestFocus();
                        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                    }
                }
            }
        });







    }
}
