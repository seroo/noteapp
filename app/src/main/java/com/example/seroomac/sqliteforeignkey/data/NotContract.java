package com.example.seroomac.sqliteforeignkey.data;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by Elitgd on 23.01.2018.
 */

public final class NotContract {

    private NotContract() {};

    public static final String CONTENT_AUTHORITY = "com.example.seroomac.sqliteforeignkey";

    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String PATH_NOT = "note";
    public static final String PATH_CAT = "category";

    public static final class NotEntry implements BaseColumns {

        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_NOT)
                .build();
        public static final String CONTENT_LIST_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" +
                CONTENT_URI + "/" + PATH_NOT;

        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" +
                CONTENT_URI + "/" + PATH_NOT;

        public static final String TABLE_NAME = "note";

        public static final String _ID = BaseColumns._ID;

        public static final String COLUMN_HEADER = "header";

        public static final String COLUMN_CONTENT = "content";

        public static final String COLUMN_CAT_ID = "categoryID";

        public static final String COLUMN_DATE = "date";

    }

    public static final class CategoryEntry implements BaseColumns {

        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_CAT)
                .build();
        public static final String CONTENT_LIST_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" +
                CONTENT_AUTHORITY + "/" + PATH_CAT;

        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" +
                CONTENT_AUTHORITY + "/" + PATH_CAT;

        public static final String TABLE_NAME = "category";

        public static final String _ID = BaseColumns._ID;

        public static final String COLUMN_CATEGORY_NAME= "categoryName";

    }


}
