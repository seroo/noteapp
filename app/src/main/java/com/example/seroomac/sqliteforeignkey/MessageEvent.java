package com.example.seroomac.sqliteforeignkey;

/**
 * Created by seroo on 16.03.2018.
 */

public class MessageEvent {

    public static class Response {
        private int isFromInApp;

        public Response(int isFromInApp) {
            this.isFromInApp = isFromInApp;
        }

        public int getIsFromInApp() {
            return isFromInApp;
        }

    }
}
