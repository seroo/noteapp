package com.example.seroomac.sqliteforeignkey.Utils;

import android.app.Activity;
import android.app.Dialog;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.seroomac.sqliteforeignkey.MessageEvent;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 */

public class FileChooser {
    private static final String PARENT_DIR = "..";
    /**
     * BU CLASS A GEREK KALMADI SİL
     */

    private Activity activity;
    private ListView list;
    private Dialog dialog;
    private File currentPath;
    private FileSelectedListener fileListener;

    //filter on file extension
    private String extension = null;
    public void setExtension(String extension) {
        this.extension = (extension == null) ? null : extension.toLowerCase();
    }

    //file selection event handling

    public interface FileSelectedListener {
        void fileSelected(File file);
    }
    public FileChooser setFileListener(FileSelectedListener fileListener) {
        this.fileListener = fileListener;
        return this;
    }
    public FileChooser(Activity activity) {
        this.activity = activity;
        dialog = new Dialog(activity);
        list = new ListView(activity);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int which, long id) {
                String fileChoosen = (String) list.getItemAtPosition(which);
                File chosenFile = getChosenFile(fileChoosen);
                if(chosenFile.isDirectory()) {
                    refresh(chosenFile);
                } else {
                    if(fileListener != null) {
                        fileListener.fileSelected(chosenFile);
                    }
                    dialog.dismiss();
                }
            }
        });
        dialog.setContentView(list);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
       // refresh(Environment.getExternalStorageDirectory());
    }

    private void refresh(File path) {
        this.currentPath = path;
        if (path.exists()) {
            File[] dirs = path.listFiles(new FileFilter() {
                @Override
                public boolean accept(File file) {
                    return (file.isDirectory() && file.canRead());
                }
            });
            File[] files = path.listFiles(new FileFilter() {
                @Override
                public boolean accept(File file) {
                    if (!file.isDirectory()) {
                        if (!file.canRead()) {
                            return false;
                        } else if (extension == null) {
                            return true;
                        } else {
                            return file.getName().toLowerCase().endsWith(extension);
                        }
                    } else {
                        return false;
                    }
                }
            });

            //convert to an array
            int i = 0;
            int j = 0;
            int k = 0;
            if(dirs!=null && dirs.length>0)
                j = dirs.length;
            if(files!=null && files.length>0)
                k = files.length;
            String[] fileList;
            if (path.getParentFile() == null) {
                fileList = new String[j + k];
            } else {
                //fileList = new String[dirs.length + files.length + 1];
                fileList = new String[j + k + 1];
                fileList[i++] = PARENT_DIR;
            }
            if(dirs!=null && dirs.length>0) {
                Arrays.sort(dirs);
                for (File dir : dirs) { fileList[i++] = dir.getName(); }}
            if(files!=null && files.length>0){
                Arrays.sort(files);
                for (File file : files ) { fileList[i++] = file.getName(); }}

            //refresh the user interface
            dialog.setTitle(currentPath.getPath());
            list.setAdapter(new ArrayAdapter(activity, android.R.layout.simple_list_item_1,
                    fileList){
                @NonNull
                @Override
                public View getView(int pos, @Nullable View view, @NonNull ViewGroup parent) {
                    view = super.getView(pos, view, parent);
                    ((TextView) view).setSingleLine(true);
                    return view;
                }
            });
            }
    }

    private File getChosenFile(String fileChoosen) {
        if (fileChoosen.equals(PARENT_DIR)) {
            return currentPath.getParentFile();
        } else {
            return new File(currentPath, fileChoosen);
        }
    }

    public void showDialog() {
        dialog.show();
    }



}
