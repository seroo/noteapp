package com.example.seroomac.sqliteforeignkey;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.seroomac.sqliteforeignkey.data.DatabaseHelper;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.List;

import easyfilepickerdialog.kingfisher.com.library.model.DialogConfig;
import easyfilepickerdialog.kingfisher.com.library.view.FilePickerDialogFragment;

public class ImportDatabase extends AppCompatActivity {

    TextView tvPath;
    Button btnChoose;
    Button btnApply;
    Context context;
    String mPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_import_database);

        tvPath = findViewById(R.id.tv_path);
        btnChoose = findViewById(R.id.button_choose_file);
        btnApply = findViewById(R.id.button_apply);

        Intent intent = getIntent();
        context = getBaseContext();


    }

    public void chooseFile(View view) {
        mPath = "";
        DialogConfig dialogConfig = new DialogConfig.Builder()
                .build();
        new FilePickerDialogFragment.Builder()
                .configs(dialogConfig)
                .onFilesSelected(new FilePickerDialogFragment.OnFilesSelectedListener() {
                    @Override
                    public void onFileSelected(List<File> list) {
                        mPath = list.get(0).getAbsolutePath();
                        tvPath.setText(mPath);
                        Log.e("ONFILESELECTED", mPath);
                        //  Toast.makeText(MainActivity.this, mChosenFilePath, Toast.LENGTH_LONG).show();
                    }
                }).build().show(getSupportFragmentManager(), null);
        Log.e("BEFORE RETURN", mPath);

    }

    @Override
    public void onBackPressed() {
        EventBus.getDefault().post(new MessageEvent.Response(1));
        super.onBackPressed();
    }

    public void apply(View view) {
        try {
            File targetDbPathFile = Environment.getDataDirectory();
            File targetDbFile = new File(targetDbPathFile, DatabaseHelper.DB_FILE_PATH);//mevcut .db dosyasi
            File choosenFile = new File(mPath); //secilen .db dosyası
            Log.e("BEFORE FILE COPY", mPath);
            FileChannel src = new FileInputStream(choosenFile).getChannel();
            //FIXME-cozulmus burada hala diske yazma izni yok elle izin verip burayi geciyorum.. daha sonra duzelt buralari
            FileChannel dst = new FileOutputStream(targetDbFile).getChannel();
            dst.transferFrom(src, 0, src.size());
            src.close();
            dst.close();
            Toast.makeText(getBaseContext(), "Successfully import Database", Toast.LENGTH_SHORT).show();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
            Log.e("ERROR", ex.getStackTrace().toString());
        }
        setResult(RESULT_OK);
        EventBus.getDefault().post(new MessageEvent.Response(1));
        finish();

    }
}
