package com.example.seroomac.sqliteforeignkey;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;
import com.example.seroomac.sqliteforeignkey.data.NotContract.NotEntry;
import com.example.seroomac.sqliteforeignkey.data.NotContract.CategoryEntry;

/**
 * Created by seroomac on 27.01.2018.
 */

public class CategoryListAdapter extends CursorAdapter {


    public CategoryListAdapter(Context context, Cursor c) {
        super(context, c, 0);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.list_item, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        TextView tv = (TextView) view.findViewById(R.id.category_name);

        String categoryName = cursor.getString(cursor.getColumnIndexOrThrow(
                CategoryEntry.COLUMN_CATEGORY_NAME));

        tv.setText(categoryName);


    }
}
