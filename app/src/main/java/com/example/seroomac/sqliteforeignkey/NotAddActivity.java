package com.example.seroomac.sqliteforeignkey;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.seroomac.sqliteforeignkey.data.NotContract;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class NotAddActivity extends AppCompatActivity implements android.support.v4.app.LoaderManager.LoaderCallbacks<Cursor>{

    EditText etHeader;
    EditText etContent;
    Spinner mSpinner;
    ArrayAdapter<String> mDataAdapter;
    Uri mCurrentNoteUri;
    boolean isAnExistedNoteOrBlank;
    private static final int EXISTING_NOTE_LOADER = 0;
    boolean isSaveNoteClicked;
    boolean fromDeleteMenuItem;
    ActionBar actionBar;
    //yapildi TO-DO Date'in alinmasinda sikinti var database de integer gorunuyor ama biz oradan cekmioz cok karismis
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fromDeleteMenuItem = false;
        isSaveNoteClicked = false;
        isAnExistedNoteOrBlank = false;
        setContentView(R.layout.activity_not_add);
        mSpinner = findViewById(R.id.spinner_categories);
        etContent = findViewById(R.id.et_not_add_content);
        etHeader = findViewById(R.id.et_not_add_header);
//        actionBar = getSupportActionBar();
//        actionBar.show();
        loadSpinner();
        Intent intent = getIntent();
        String mCurrentCategoryName = intent.getStringExtra("categoryName");
        mCurrentNoteUri = intent.getData();
        if (mCurrentNoteUri != null) {
            String pathOfUri = mCurrentNoteUri.getPath();
            if (pathOfUri.charAt(1) == NotContract.PATH_CAT.charAt(0)) {
                //yapildi FIX-ME Burada category adini spinner a sectir

                mSpinner.setSelection(mDataAdapter.getPosition(mCurrentCategoryName));

            } else {
                // null degilse demekki direk category ekranindan not eklenerek gelinmis
                getSupportLoaderManager().initLoader(EXISTING_NOTE_LOADER, null, this);
                isAnExistedNoteOrBlank = true;
            }

        }
    }

    @Override
    public void onBackPressed() {
        EventBus.getDefault().post(new MessageEvent.Response(1));
        super.onBackPressed();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu options from the res/menu/menu_editor.xml file.
        // This adds menu items to the app bar.
        getMenuInflater().inflate(R.menu.menu_add_not_options, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        // User clicked on a menu option in the app bar overflow menu
        switch (item.getItemId()) {
            // Respond to a click on the "Save" menu option
            case R.id.action_save:
                saveNot();
                isSaveNoteClicked = true;
                EventBus.getDefault().post(new MessageEvent.Response(1));
                finish();
                return true;
            // Respond to a click on the "Delete" menu option
            case R.id.action_delete:

                showDeleteConfirmationDialog();

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void saveNot() {

        String header = etHeader.getText().toString();
        String noteContent = etContent.getText().toString();
        if (header.equals("") && noteContent.equals("") && !(isAnExistedNoteOrBlank)) {
            return;
        }
        String categoryName = mSpinner.getSelectedItem().toString();
        long catId = findCatId(categoryName);
        Calendar c = new GregorianCalendar();
        Long dateInLong = c.getTimeInMillis();

        ContentValues values = new ContentValues();
        values.put(NotContract.NotEntry.COLUMN_HEADER, header);
        values.put(NotContract.NotEntry.COLUMN_CONTENT, noteContent);
        values.put(NotContract.NotEntry.COLUMN_DATE, dateInLong);
        values.put(NotContract.NotEntry.COLUMN_CAT_ID, catId);

        if (!isAnExistedNoteOrBlank) {
            Uri newUri = getContentResolver().insert(NotContract.NotEntry.CONTENT_URI, values);
            if (newUri == null) {
                Toast.makeText(this, "Save Failed", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Save successfull", Toast.LENGTH_SHORT).show();
            }
        }else {
          //  long id = ContentUris.parseId(mCurrentNoteUri);
           // Uri newUri = ContentUris.withAppendedId(NotContract.NotEntry.CONTENT_URI, id);
            int rowsAffected = getContentResolver().update(mCurrentNoteUri, values, null, null);
            if (rowsAffected == 0) {
                Toast.makeText(this, "Update Failed", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Successfully updated", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private long findCatId(String categoryName) {
        long id = -1;

        Cursor cursor = getCategoryCursor();
        int idIndex = cursor.getColumnIndex(NotContract.CategoryEntry._ID);
        int categoryNameIndex = cursor.getColumnIndex(NotContract.CategoryEntry.COLUMN_CATEGORY_NAME);

        if (cursor.moveToFirst()) {
            do {
                String cursorCategoryName = cursor.getString(categoryNameIndex);
                if (cursorCategoryName.equals(categoryName)) {
                    return cursor.getLong(idIndex);
                }
            } while (cursor.moveToNext());
        }
        return id;
    }

    private Cursor getCategoryCursor() {

        String[] projection = {
            NotContract.CategoryEntry._ID,
                    NotContract.CategoryEntry.COLUMN_CATEGORY_NAME
        };

        return getContentResolver().query(NotContract.CategoryEntry.CONTENT_URI, projection, null,
                null, null);
    }

    private void showDeleteConfirmationDialog() {
        // Create an AlertDialog.Builder and set the message, and click listeners
        // for the postivie and negative buttons on the dialog.
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Emin misiniz?");
        builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                if (mCurrentNoteUri == null) {
                    EventBus.getDefault().post(new MessageEvent.Response(1));
                    finish();
                } else {
                    deleteNote();
                    EventBus.getDefault().post(new MessageEvent.Response(1));
                    finish();
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked the "Cancel" button, so dismiss the dialog
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

        // Create and show the AlertDialog
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    private void deleteNote() {
        int rowsDeleted = getContentResolver().delete(mCurrentNoteUri, null, null);
        //kac row silindiginin sayisini doner
        if (rowsDeleted == 0) {
            Toast.makeText(this, "Delete Failed", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Note deleted", Toast.LENGTH_SHORT).show();
        }
        fromDeleteMenuItem = true;
    }

    private void loadSpinner() {

        ArrayList<String> categoryList = new ArrayList<>();
        Cursor cursor = getContentResolver().query(NotContract.CategoryEntry.CONTENT_URI, null,
                null, null, null);
        int categoryNameIndex = cursor.getColumnIndex(NotContract.CategoryEntry.COLUMN_CATEGORY_NAME);
        if (cursor.moveToFirst()) {
            do {
               categoryList.add(cursor.getString(categoryNameIndex));
            } while (cursor.moveToNext());
        }
        mDataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, categoryList);

        mDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(mDataAdapter);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        String[] projection = {
                NotContract.NotEntry._ID,
                NotContract.NotEntry.COLUMN_CONTENT,
                NotContract.NotEntry.COLUMN_HEADER,
                NotContract.NotEntry.COLUMN_CAT_ID,
        };

        return new CursorLoader(this, mCurrentNoteUri, projection, null,
                null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {

        if (cursor == null || cursor.getCount() < 1) {
            return;
        }

        if (cursor.moveToFirst()) {
            int contentIndex = cursor.getColumnIndex(NotContract.NotEntry.COLUMN_CONTENT);
            int headerIndex = cursor.getColumnIndex(NotContract.NotEntry.COLUMN_HEADER);
            int catIdIndex = cursor.getColumnIndex(NotContract.NotEntry.COLUMN_CAT_ID);
            String content = cursor.getString(contentIndex);
            String header = cursor.getString(headerIndex);
            long catId = cursor.getLong(catIdIndex);
            String catName = findCatName(catId);
            int spinnerPosition = mDataAdapter.getPosition(catName);
            mSpinner.setSelection(spinnerPosition);
            etHeader.setText(header);
            etContent.setText(content);

        }
    }

    private String findCatName(long catId) {

        String name = "";
        Cursor cursor = getCategoryCursor();
        int idIndex = cursor.getColumnIndex(NotContract.CategoryEntry._ID);
        int categoryNameIndex = cursor.getColumnIndex(NotContract.CategoryEntry.COLUMN_CATEGORY_NAME);

        if (cursor.moveToFirst()) {
            do {
                long cursorCategoryId = cursor.getLong(idIndex);
                if (cursorCategoryId == catId) {
                    return cursor.getString(categoryNameIndex);
                }
            } while (cursor.moveToNext());
        }
        return name;
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        etHeader.setText("");
        etContent.setText("");
        mSpinner.setSelection(0);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (isSaveNoteClicked) {
            //do nothing
        } else if (fromDeleteMenuItem){
           return;
        } else {
            if (etHeader.getText().toString().equals("") && etContent.getText().toString().equals("")) {
                //do nothing
            } else {
                Toast.makeText(this, "--Auto saved your note--", Toast.LENGTH_SHORT).show();
                saveNot();
                finish();
            }
        }
        onStop();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }
}


